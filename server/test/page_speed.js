/** this to ensure that we will use the mock database */
import { API_KEY } from "../src/config/env.js";
import { getPerformanceFromApi, savePerformance } from "../src/components/page_speed/page_speed.controller.js";
import { assert, expect, should } from "chai";
import { connect } from "../src/config/database.js";

describe("page_speed.controller.js Unit Tests", () => {
    const URL_TEST = "https://www.google.com"
    describe("page_speed.getPerformanceFromApi() Tests", () => {
        describe("getPerformanceFromApi() Validity Tests", () => {
            it("should return a valid response with the performance", () => {
                const performance = getPerformanceFromApi(API_KEY, URL_TEST)
                assert(performance)
            });
        });
        describe('getPerformanceFromApi() Invalidity Tests', () => {
            it('should return undefined  when url is not defined ', () => {
                const performance = getPerformanceFromApi(API_KEY, undefined)
                should(performance, undefined);
            });
            it('should return undefined  when url is empty ', () => {
                const performance = getPerformanceFromApi(API_KEY, '')
                should(performance, undefined);
            });
            it('should return undefined  when Api_Key is not defined ', () => {
                const performance = getPerformanceFromApi(undefined, URL_TEST)
                should(performance, undefined);
            });
            it('should return undefined  when Api_Key is empty ', () => {
                const performance = getPerformanceFromApi('', URL_TEST)
                should(performance, undefined);
            });
        });

    });
    describe("page_speed.savePerformance() Tests", () => {
        describe("savePerformance() Validity Tests", () => {
            // using mock database - see config/database.js
            process.env.NODE_ENV = "test"
            connect()
            it("should save an object in the database", () => {
                const performanceToSave = {
                    "title": "Performance",
                }
                // save the performance object to the mock database
                const result = savePerformance(performanceToSave)
                expect(result).to.equal(1)

            })
        })
        describe("savePerformance() Invalidity Tests", () => {
            it("should return error when the object is undefined", () => {
                const result = savePerformance(undefined)
                expect(result).to.equal(-1)

            })
            it("should return error when the object is empty", () => {
                const result = savePerformance({})
                expect(result).to.equal(-1)

            })
        })
    })
});

