import mongoose from 'mongoose'
import { MONGO_URL } from './env.js'
import { getPrintableDbConnectionString } from '../shared/mongoUtils.js'
import { Mockgoose } from 'mockgoose';


/** connect to real/mock database */
export function connect() {
    if (process.env.NODE_ENV === 'test') {
        const mockgoose = new Mockgoose(mongoose)
        mockgoose.prepareStorage()
            .then(() => {
                mongoose.connect("Fake Database").then(result => {
                    console.log("✓  - Page Speed SVC - Successfully connected to Fake Database ")
                }).catch(error => {
                    getPrintableDbConnectionString
                    console.log("✕ - Page Speed SVC - Failed to connect to Fake Database")
                    console.log("✕ - Page Speed SVC - ", error);
                });

            })

    } else {
        mongoose.connect(MONGO_URL).then(result => {
            console.log("✓  - Page Speed SVC - Successfully connected to: ", getPrintableDbConnectionString(MONGO_URL))
        }).catch(error => {
            console.log("✕ - Page Speed SVC - Failed to connect to:", getPrintableDbConnectionString(MONGO_URL))
            console.log("✕ - Page Speed SVC - ", error);
        });

    }


}
