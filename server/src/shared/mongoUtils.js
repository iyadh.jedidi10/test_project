export function getPrintableDbConnectionString(connectionString){
    if (connectionString?.includes('@') && connectionString?.includes(':')){
        return connectionString.split('@')[1].split(':')[0];
    }
    return connectionString;
}
