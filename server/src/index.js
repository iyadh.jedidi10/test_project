
import express from 'express'
import { connect } from './config/database.js'
import routes from './routes/routes.js'
import { PORT } from './config/env.js'
import bodyParser from 'body-parser'
import cors from 'cors'
import cron from "node-cron"
import { cronForGetPerformance } from './components/cron_job/cron_job.controller.js'
process.env.NODE_ENV = "test"


process.env.NODE_ENV = "test"
connect()
const app = express();

app.use(bodyParser.json());

app.use('/api', cors(), routes);

cron.schedule('* * * * *', async function () {
    await cronForGetPerformance()
});

app.listen(PORT, () => {
    console.log(`✓ - Successfully started application on port ${PORT}`)
})