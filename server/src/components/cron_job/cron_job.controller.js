import { getPerformanceFromApi, savePerformance } from "../page_speed/page_speed.controller.js";
import { API_KEY, URL } from "../../config/env.js";

/**
 * call all the functions that will be executed in the cronjob
 * @returns {Object}
**/
export async function cronForGetPerformance() {
    try {
        const performance = await getPerformanceFromApi(API_KEY, URL);
        const insertResult = savePerformance(performance);
        if (insertResult) {
            return { code: 200, data: "Successfully inserted in the database" };
        } else {
            return { code: 500, data: "Please make sure that you entered the right object" };

        }

    } catch (error) {
        throw Error(error);
    }
}
