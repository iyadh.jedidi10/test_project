import { cronForGetPerformance } from "./cron_job.controller.js";

export default function (router) {
    router.get("/cron-job", async (req, res) => {
        try {
            const result = await cronForGetPerformance();
            return res.status(result.code).json(result.data);
        } catch (error) {
            return res.status(500).json(error);
        }
    });
}