import psi from "psi";
import performanceSchema from "./page_speed.js";

/**
 * Get the performance from the api
 * @param {String} api_key obtained from google [https://developers.google.com/speed/docs/insights/v5/get-started]
 * @param {String} url the website that we are trying to measure its performance 
 * @returns {Object} contain the performance of the website
**/

export async function getPerformanceFromApi(api_key, url) {
  try {
    if (!api_key || !url || api_key.length == 0 || url.length == 0) {
      return undefined
    }
    const opts = {
      key: api_key,
    };
    const result = await psi(url, opts);
    return result.data.lighthouseResult.categories.performance;

  } catch (error) {
    throw Error("API problem :", error);
  }
}

/**
 * Save the performance of the website in the database [prisma-media]
 * @param {Object} obj the object that we will inserted
 * @returns {Number} indicate that the data is inserted or not in the database
**/
export function savePerformance(obj) {
  try {
    if (!obj || Object.keys(obj).length == 0) {
      return -1
    }
    obj.date = Date.now();
    const performance = new performanceSchema(obj);
    performance.save();
    return 1;
  } catch (error) {
    throw Error(error);
  }
}

/**
 * Save the performance of the website in the database [prisma-media]
 * @param {String} attr the requested field in the data
 * @returns {Object} the answer for the page_speed.routes.js file
**/
export async function getAllPerformancesByAttr(attr) {
  try {
    const performances = await performanceSchema.find();
    if (performances.length != 0) {
      let result = []
      for (let perf of performances) {
        const date_of_perf = new Date(perf.date);
        const date_formatter = date_of_perf.toString().substr(0, 21);
        if (attr == "score") {
          result.push([date_formatter, perf.score]);

        } else {
          let res = perf.auditRefs?.filter(doc => doc.id == attr)[0];
          result.push([date_formatter, res.weight]);
        }
      }
      return { code: 200, data: result };
    } else {
      return { code: 204, data: "Empty Data" };
    }

  } catch (error) {
    throw Error(error);
  }
}


