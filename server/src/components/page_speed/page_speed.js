import mongoose from 'mongoose'
const Schema = mongoose.Schema;

const auditRefsSchema = new Schema({
    id: { type: String },
    weight: { type: Number },
    group: { type: String },
    acronym: { type: String },
    relevantAudits: [{ type: String }]
})
const performanceSchema = new Schema(
    {
        id: { type: String },
        title: { type: String },
        date: { type: Date },
        score: { type: Number },
        auditRefs: [{ type: auditRefsSchema }]
    });
export default mongoose.model('performance', performanceSchema);
