import { getAllPerformancesByAttr } from "./page_speed.controller.js";

export default function (router) {
  router.get("/performances/:attr", async (req, res) => {
    try {
      const result = await getAllPerformancesByAttr(req.params.attr);
      return res.status(result.code).json(result.data);
    } catch (error) {
      return res.status(500).json(error);
    }
  });
}
