/** This file is used to group all the routes of the different components of the project **/

import express from "express";
const router = express.Router();

import pagespeedRoutes from "../components/page_speed/page_speed.routes.js";
import cronJobRoutes from "../components/cron_job/cron_job.routes.js";


pagespeedRoutes(router);
cronJobRoutes(router);

export default router;
