# Node server for Page Speed API

## Install

1. Install [NodeJS]
2. Install [MongoDB]
3. Make sure that MongoDB run on port 27017 otherwise you should change it in the database.js file
4. Apply the commands below in the "server" directory

```
npm install
```

## start

    npm start

## Test

    npm test

[nodejs]: https://nodejs.org/en/
[mongodb]: https://www.mongodb.com/
