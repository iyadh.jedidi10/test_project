import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PerformanceService {

  readonly baseUrl = environment.backendBaseUrl + '/performances/';

  constructor(private httpClient: HttpClient) {
  }

  getAllPerformances(attr): Promise<any> {
    return this.httpClient.get(this.baseUrl + attr).toPromise();
  }
}
