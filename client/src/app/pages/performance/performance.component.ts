import { Component, OnInit } from '@angular/core';
import { PerformanceService } from 'src/app/shared/services/performance.service';

@Component({
  selector: 'app-performance',
  templateUrl: './performance.component.html',
  styleUrls: ['./performance.component.css']
})
export class PerformanceComponent implements OnInit {
  type = 'LineChart';
  dataScore: any;
  dataLargestContentfulPaint: any;
  dataUnminifiedJavascript: any;
  columnScoreNames = ['Date', 'Score'];
  columnLargestContentfulPaintNames = ['Date', 'Largest Content ful Paint'];
  columnUnminifiedJavascriptNames = ['Date', 'Unminified Javascript'];

  width = 800;
  height = 600;

  constructor(private performanceService: PerformanceService) { }

  async ngOnInit() {
    console.log("hi")
    await this.getPerfermanceScore();
    await this.getPerfermanceLargestContentfulPaint();
    await this.getPerfermanceUnminifiedJavascript();
  }

  async getPerfermanceLargestContentfulPaint() {

    this.dataLargestContentfulPaint = await this.performanceService.getAllPerformances('largest-contentful-paint');
  }

  async getPerfermanceScore() {

    this.dataScore = await this.performanceService.getAllPerformances('score');
  }

  async getPerfermanceUnminifiedJavascript() {

    this.dataUnminifiedJavascript = await this.performanceService.getAllPerformances('unminified-javascript');
  }

}
