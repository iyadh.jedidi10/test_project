import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PerformanceComponent } from './pages/performance/performance.component';


const routes: Routes = [
  { path: '', component: PerformanceComponent },
  { path: 'performance', component: PerformanceComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
